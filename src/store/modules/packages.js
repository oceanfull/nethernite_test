import api from '@/helpers/api';

export default {
  namespaced: true,
  state: {
    items: [],
    total: 0,
    loading: false,
    packageItem: {},
    loadingOne: false
  },
  mutations: {
    setList(state, payload) {
      state.items = payload
    },
    setTotal(state, payload) {
      state.total = payload
    },
    setLoadingList(state, payload) {
      state.loading = payload
    },
    setLoadingPackage(state, payload) {
      state.loadingOne = payload
    },
    setPackage(state, payload) {
      state.packageItem = payload
    }
  },
  actions: {
    async getList({ commit }, payload) {
      commit('setLoadingList', true)
      let result = await api.get.packagesList(payload)
      commit('setList', result.objects)
      commit('setTotal', result.total)
      commit('setLoadingList', false)
    },
    async getPackage({ commit }, payload) {
      commit('setLoadingPackage', true)
      let result = await api.get.package(payload)
      commit('setPackage', result)
      commit('setLoadingPackage', false)
    }
  }
}

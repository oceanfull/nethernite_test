const NPM_REGISTRY_SEARCH = 'https://registry.npmjs.org/-/v1/search';
const JSDELIVER_MORE_INFO = 'https://data.jsdelivr.com/v1/package/npm';
const parseJsonResponse = response => {
  if (response.ok) {
    return response.json();
  } else {
    throw new Error('Something went wrong');
  }
};

export default {
  get: {
    packagesList(payload) {
      return middle({
        path: NPM_REGISTRY_SEARCH,
        method: 'GET',
        payload
      })
    },
    package(payload) {
      return middle({
        path: JSDELIVER_MORE_INFO,
        method: 'GET',
        payload,
        inlineParam: true
      })
    }
  }
}

function middle({
  path,
  method,
  payload,
  inlineParam
}) {
  return new Promise((res) => {

    let urlParams = '';

    const options = {}

    const isGet = method === 'GET';

    if (!isGet) {
      options.method = method;
      options.body = JSON.stringify(payload);
    } else {
      const stringParams = new URLSearchParams(payload).toString();
      urlParams = stringParams ? '?' + stringParams : '';
    }

    if (inlineParam) {
      urlParams = '/' + payload
    }

    const fullPath = path + urlParams;

    fetch(fullPath, options)
      .then(parseJsonResponse)
      .then(res).catch(e => {
        res({ error: true, message: e })
      });

  })
}


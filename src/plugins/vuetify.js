import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#F1B2FF", // #E53935
        secondary: "#F1B2FF", // #FFCDD2
        accent: "#F1B2FF", // #3F51B5
      }
    },
  }
});
